# Scientific edition:
***Open source, free et collaborative scientific edition aternative***

### Why do we need an alternative ?
> *We need an online tool that makes it easy for scholars to write scholar and academic documents (papers, submissions answers, posters,slides, bibliography, experiments) in a **collaborative** manner.* 

> We need to ease full chain of scientific edition 

Academic writing involves :

* writing down ideas as they come along and documenting results (notetaking),
* experimenting with these ideas (simulations and data analysis)
* finally presenting them effectively (scientific paper)
    * Call for submission
    * Draft
    * Proposal
    * Review by peer
    * Format in the corresponding journal

A whole and heavy process to make sciencitic work : a full history of editions with comments, suggestions, modifications and validations that could be chained


### Toolchain: Markdown + Git + Zotero

##### Markdown
###### Why Markdown?

* Easy: the syntax is simple
* Fast: the simple formatting saves time and speeds up workflows of writers
* Portable: documents are cross-platform by nature
* Flexible: HTML, PDF, DOCX, TEX are all supported output formats

###### Markdown in 10 minutes

* [Markdown Syntax](./markdown.md)

* [Mardown in Practise](./markdown-advanced.md)
* [Tools for editing markdown]( https://github.com/scholmd/scholmd/wiki/Tools-to-support-your-markdown-authoring)

##### Git
###### Why Git?

* Version management: follow the full history of your article
* Verification & Reliability: Find and fix bugs.
* Transparency: Increased citation count, broader impact, institutional memory
* Efficiency: Reduces duplication of effort, integration of comments modifications revisions on the fly
* Flexibility: fine grain control of roles, permission and publication


###### Git in 10 minutes
* [Git basics](./git.md)
* [Git academic workflow](./git-advanced.md)

##### Zotero
###### Why Zotero?

* Ease citation management
* Store in one place your bibliographical information
* Create the citation style you need for your journal or your specific discipline

###### Zotero in 10 minutes

* [Zotero basics](https://github.com/HackYourPhd/ateliers-open-geek/blob/master/Atelier_05_biblio_zotero/Atelier_05.md)
* [Zotero advanced](https://github.com/HackYourPhd/ateliers-open-geek/blob/master/Atelier_13_zotero_csl/atelier_13.md)

##### A quick word on special cases

* Maths
* Economy
* DataScience and Statistics
* Computer Science 
* Litterature, History, Sociology

